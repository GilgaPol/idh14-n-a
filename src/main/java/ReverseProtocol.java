package main.java;

public class ReverseProtocol {
    private static final int WAITING = 0;;
    private static final int SENTSENTENCE = 1;
    private static final int ANOTHER = 2;

    private static final int NUMTIMES = 5;

    private int state = WAITING;
    private int currentTime = 1;

    private ReverseProtocolService sentSentenceService;

    public void setSentSentenceService(ReverseProtocolService object) {
        sentSentenceService = object;
    }

    public String processInput(String theInput) {
        String theOutput = null;

        //Waiting for someone to connect
        if (state == WAITING) {
            theOutput = "Hello!";
            //Acknowledged connection
            state = SENTSENTENCE;
        //Waiting for sentence to reverse
        } else if (state == SENTSENTENCE) {
            String[] parts = theInput.split(":", 2);
            if (parts[0].equalsIgnoreCase("Text")) {
                theOutput = "";
                if(sentSentenceService != null){
                    theOutput = sentSentenceService.sentSentence(parts[1]);
                }
                theOutput = theOutput.concat(". Want to try again? (y/n)");
                state = ANOTHER;
            } else {
                theOutput = "You're supposed to say \"Text:...\"! " +
                        "Try again. Hello!";
            }
        //Waiting if another round should be started
        } else if (state == ANOTHER) {
            if (theInput.equalsIgnoreCase("y") && currentTime != NUMTIMES) {
                theOutput = "Hello!";
                currentTime++;
                state = SENTSENTENCE;
            } else {
                theOutput = "Bye.";
                state = WAITING;
            }
        }
        return theOutput;
    }
}
