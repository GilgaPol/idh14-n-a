package main.java;

public class Reverse implements ReverseProtocolService {

    public String sentSentence(String input){
        return reverseString(input);
    }

    /**
     * Reverses the input string
     * @param input A string to reverse
     * @return The reversed input string
     */
    private String reverseString(String input){
        if(input.length() == 0) {
            return input;
        }
        byte [] strAsByteArray = input.getBytes();

        byte [] result = new byte [strAsByteArray.length];

        // Store result in reverse order into the
        // result byte[]
        for (int i = 0; i<strAsByteArray.length; i++)
            result[i] = strAsByteArray[strAsByteArray.length-i-1];
        return new String(result);
    }
}
